﻿using FluentNHibernate.Mapping;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mappings
{
    public class RentalMap : ClassMap<Rental>
    {
        public RentalMap()
        {
            Id(m => m.Id);
            Map(m => m.RentalDate);
            Map(m => m.ReturnDate);
            Map(m => m.DueDate);
            HasOne(m => m.Rating).PropertyRef(m => m.Rental);
            References<Customer>(s => s.Customer, "Customer_Id").Cascade.All();
              References<Video>(s => s.Video, "Video").Cascade.All();// maybe video_ID
        }
    }
}
