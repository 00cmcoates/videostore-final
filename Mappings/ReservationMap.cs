﻿using FluentNHibernate.Mapping;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mappings
{
    class ReservationMap : ClassMap<Reservation>
    {
        public ReservationMap()
        {
            Id(a => a.Id);
            Map(x => x.ReservationDate);
            References<Movie>(x => x.Movie, "Movie_Id");
            References(c => c.Customer, "Customer_Id").Unique();
        }
    }
}
