﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Model;

namespace Mappings
{
    public class RatingMap : ClassMap<Rating>
    {
        public RatingMap()
        {
            Id(c => c.Id);
            Map(c => c.Comment);
            Map(c => c.Score);
            References(c => c.Rental, "Rental_Id").Unique();
        }
    }
}
