﻿using FluentNHibernate.Mapping;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mappings
{
    public class CommunicationMethodMap : ClassMap<CommunicationMethod>
    {
        public CommunicationMethodMap()
        {
            Id(m => m.Name).GeneratedBy.Assigned();
            Map(m => m.Frequency);
            Map(m => m.Units);
            HasManyToMany<Customer>(c => c.Customers)
                .Table("PrefersComm")
                .Inverse()
                .Cascade.All();
        }
        
    }
}
