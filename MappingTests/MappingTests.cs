﻿using NHibernate;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using Mappings;
using FluentNHibernate.Testing;
using VideoStore.Utilities;
using System.Collections;

namespace MappingTests
{
    public class MappingTests
    {
        private ISession _session;
        private Movie starWars;
        private Movie hoosiers;
        private Genre action;
        private ZipCode zeeland;
        private Name nam;
                        
        [SetUp]
        public void CreateSession ()
        {
            _session = SessionFactory.CreateSessionFactory().GetCurrentSession();
            _session.CreateSQLQuery("delete from videostore.Area").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.Rental").ExecuteUpdate(); // Moved ahead of "delete from videostore.Customer because customer depends on rentsl, that's why bottom 4 tests did not work
            _session.CreateSQLQuery("delete from videostore.Customer").ExecuteUpdate();
            
            _session.CreateSQLQuery("delete from videostore.Video").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.Employee").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.Store").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.ZipCode").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.Rating").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.Reservation").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.CommunicationMethod").ExecuteUpdate();
            _session.CreateSQLQuery("delete from imdb.Genres where Genre = 'Action2'").ExecuteUpdate();

            starWars = _session.Load<Movie>("tt0076759 ");
            var title = starWars.Title;
            
            
            hoosiers = _session.Load<Movie>("tt0091217 ");
            title = hoosiers.Title;

            action = _session.Load<Genre>("Action");
            String name = action.Name;

            zeeland = new ZipCode
            {
                Code = "49464",
                City = "Zeeland",
                State = "Michigan"
            };

            nam = new Name()
            {
                Title = "Dr.",
                First = "Ryan",
                Middle = "L",
                Last = "McFall",
                Suffix = "Sr"
            };
        }

        [Test]
        public void ZipCodeMappingIsCorrect()
        {
            new PersistenceSpecification<ZipCode>(_session)
                .CheckProperty(z => z.Code, "49464")
                .CheckProperty(z => z.City, "Zeeland")
                .CheckProperty(z => z.State, "Michigan")
                .VerifyTheMappings();
        }

        [Test]
        public void AreaMappingIsCorrect()
        {
            var zipCodes = new List<ZipCode>
            {
                zeeland,
                new ZipCode
                {
                    Code = "48444",
                    City = "Imlay City",
                    State = "Michigan"
                }
            };

            new PersistenceSpecification<Area>(_session, new DateEqualityComparer())
                .CheckProperty(a => a.Name, "Zeeland")                
                .CheckBag(a => a.ZipCodes, zipCodes)
                .VerifyTheMappings();
        }
                       
        [Test]
        public void CustomerMappingIsCorrect()
        {
           

            var stores = new List<Store>
            {
                new Store
                {
                    StoreName = "Store 2",
                    StreetAddress = "1234 Winterwood Lane",
                    PhoneNumber = "616-748-9715",
                    ZipCode = zeeland
                },
                new Store
                {
                    StoreName = "Store 1",
                    StreetAddress = "1260 Winterwood Lane",
                    PhoneNumber = "616-123-4567",
                    ZipCode = zeeland
                },
            };

            new PersistenceSpecification<Customer>(_session)
                .CheckProperty(c => c.Name, nam)
                .CheckProperty(c => c.EmailAddress, "mcfall@hope.edu")
                .CheckProperty(c => c.StreetAddress, "27 Graves Place VWF 220")
                .CheckProperty(c => c.Password, "Abc123$!")
                .CheckProperty(c => c.Phone, "616-395-7952")
                .CheckReference(c => c.ZipCode, zeeland
                )
                .CheckInverseList(
                    c => c.PreferredStores, stores, 
                    (cust, store) => cust.AddPreferredStore(store)
                )
                .VerifyTheMappings();
        }

        [Test]
        public void StoreMappingIsCorrect()
        {
            var videos = new List<Video>()
            {
                new Video
                {
                    Movie = starWars,
                    NewArrival = false,
                    PurchaseDate = DateTime.Now
                },
                new Video
                {
                    Movie = hoosiers,
                    NewArrival = true, 
                    PurchaseDate = DateTime.Now.AddDays(1)
                }
            };

            new PersistenceSpecification<Store>(_session)
                .CheckProperty(s => s.StoreName, "Blockbusted")
                .CheckProperty(s => s.StreetAddress, "1234 Broke Street")
                .CheckProperty(s => s.PhoneNumber, "616-666-0000")
                .CheckReference(s => s.ZipCode, zeeland
                )
                .CheckInverseList( s => s.Videos, videos, (s, v) => s.AddVideo(v))
                .VerifyTheMappings();
        }

        [Test]
        public void VideoMappingIsCorrect()
        {
            new PersistenceSpecification<Video>(_session, new DateEqualityComparer())
                .CheckProperty(v => v.NewArrival, true)
                .CheckProperty(v => v.PurchaseDate, DateTime.Now)
                .CheckReference(v => v.Store, 
                    new Store()
                    {
                        StoreName = "Last Store",
                        StreetAddress = "1234 Broke St",
                        PhoneNumber = "616-666-0000",
                        ZipCode = zeeland
                    }
                )
                .CheckReference(v => v.Movie, starWars)
                .VerifyTheMappings();
        }

        [Test]
        public void MovieMappingIsCorrect()
        {            
            Assert.AreEqual("tt0076759 ", starWars.TitleId);
            Assert.AreEqual("Star Wars: Episode IV - A New Hope", starWars.Title);
            Assert.AreEqual("Star Wars", starWars.OriginalTitle);
            Assert.AreEqual(1977, starWars.Year);
            Assert.AreEqual(121, starWars.RunningTimeInMinutes);
            Assert.AreEqual("PG", starWars.Rating);
            Assert.AreEqual("Action", starWars.PrimaryGenre.Name);

            Console.WriteLine(starWars.Genres.Count);
            Assert.AreEqual("Action", starWars.Genres[1].Name);
            Assert.AreEqual("Adventure", starWars.Genres[2].Name);
            Assert.AreEqual("Fantasy", starWars.Genres[3].Name);

        }

        [Test]
        public void TestGenreMap()
        {
            new PersistenceSpecification<Genre>(_session)
                .CheckProperty(v => v.Name, "Action2")
                .VerifyTheMappings();
        }

        [Test]
        public void TestCommunicationMethodMap()
        {
            new PersistenceSpecification<CommunicationMethod>(_session)
                .CheckProperty(v => v.Name, "Phone")
                .CheckProperty(v => v.Frequency, 1)
                .CheckProperty(v => v.Units, CommunicationMethod.TimeUnit.Day)
                .VerifyTheMappings();
        }

        [Test]
        public void TestRatingMap()
        {

            new PersistenceSpecification<Rating>(_session)
                .CheckProperty(v => v.Score, 4)
                .CheckProperty(v => v.Comment, "Bad Movie")
                .VerifyTheMappings();
        }

        [Test]
        public void TestEmployeeMap()
        {

            Name name1 = nam;

            new PersistenceSpecification<Employee>(_session, new DateEqualityComparer())
                .CheckProperty(c => c.Name, name1)
                .CheckProperty(c => c.Username, "abc123")
                .CheckProperty(c => c.Password, "123cba")
                .CheckProperty(c => c.DateOfBirth, DateTime.Now.AddYears(-16))
                .CheckProperty(c => c.DateHired, DateTime.Now)
                .CheckReference(c => c.Store, new Store()
                {
                    StoreName = "Last Store",
                    StreetAddress = "1234 Broke St",
                    PhoneNumber = "616-666-0000",
                    ZipCode = zeeland
                })
                .VerifyTheMappings();

        }

        [Test]
        public void TestRentalMap()
        {
            new PersistenceSpecification<Rental>(_session, new DateEqualityComparer())
               .CheckProperty(c => c.DueDate, DateTime.Now.AddDays(7))
               .CheckProperty(c => c.RentalDate, DateTime.Now)
               .CheckProperty(c => c.ReturnDate, DateTime.Now)
               .CheckReference(c => c.Video, new Video()
               {
                   Store = new Store()
                   {
                       ZipCode = new ZipCode
                       {
                           Code = "49424",
                           City = "Holland",
                           State = "Michigan"
                       },
                       StoreName = "blah",
                       PhoneNumber = " 10",
                       StreetAddress = " 123 I dont care",
                       
                   },
                   PurchaseDate = DateTime.Now,
                   NewArrival = false,
                   Movie = starWars
               })
               .CheckReference(c => c.Customer, new Customer()
               {
                   Name = nam,
                   StreetAddress = "123 apple st",
                   Phone = "248 909 2112",
                   EmailAddress = "marcoAndCam@yahoo.com",
                   Password = "Woooow121",
                   ZipCode = zeeland

               })
               .VerifyTheMappings();

        }

        [Test]
        public void TestReservationMap()
        {
            new PersistenceSpecification<Reservation>(_session, new DateEqualityComparer())
               .CheckProperty(c => c.ReservationDate, DateTime.Now)
               .CheckReference(c => c.Movie, starWars)
               .VerifyTheMappings();

        }
        [Test]
        public void TestEmployeeCollectionForStoreMap()
        {
            Store str = new Store()
            {
                ZipCode = new ZipCode
                {
                    Code = "49424",
                    City = "Holland",
                    State = "Michigan"
                },
                StoreName = "blah",
                PhoneNumber = " 10",
                StreetAddress = " 123 I dont care"
            };
            _session.Save(str);
            int str_Id = str.Id;
            Employee emp1 = new Employee()
            {
                Username = "lalala",
                Password = "boohoo",
                DateHired = DateTime.Now,
                DateOfBirth = DateTime.Now.AddYears(-16),
                Name = nam,
                Store = str
            };
                Employee emp2 = new Employee()
                {
                    Username = "bahaha",
                    Password = "lmao what",
                    DateHired = DateTime.Now,
                    DateOfBirth = DateTime.Now.AddYears(-16),
                    Name = nam,
                    Store = str
                };
            _session.Save(emp1);
            _session.Save(emp2);
            _session.Evict(str);
            str = _session.Get<Store>(str_Id);
            Assert.AreEqual(emp1, str.Managers[0]);
            Assert.AreEqual(emp2, str.Managers[1]);
        }
        [Test]
        public void TestRentalsCollectionForCustomerMap()
        {
            Customer cust = new Customer()
            {
                Name = nam,
                StreetAddress = "123 apple st",
                Phone = "248 909 2112",
                EmailAddress = "marcoAndCam@yahoo.com",
                Password = "Woooow121",
                ZipCode = zeeland

            };
            _session.Save(cust);
            int cust_Id = cust.Id;
            Rental rent1 = new Rental()
            {
                DueDate = DateTime.Now.AddDays(7),
                RentalDate = DateTime.Now,
                ReturnDate = DateTime.Now,
                Customer = cust,
                Video = new Video
                {
                    Movie = starWars,
                    NewArrival = false,
                    PurchaseDate = DateTime.Now,
                    Store = new Store
                    {
                        StoreName = "Store 1",
                        StreetAddress = "1260 Winterwood Lane",
                        PhoneNumber = "616-123-4567",
                        ZipCode = zeeland
                    }
                },
        

            };
        Rental rent2 = new Rental()
        {
            DueDate = DateTime.Now.AddDays(14),
            RentalDate = DateTime.Now,
            ReturnDate = DateTime.Now,
            Customer = cust,
            Video = new Video
            {
                Movie = starWars,
                NewArrival = false,
                PurchaseDate = DateTime.Now,
                Store = new Store
                {
                    StoreName = "Store 1",
                    StreetAddress = "1260 Winterwood Lane",
                    PhoneNumber = "616-123-4567",
                    ZipCode = zeeland
                }
            },

        };
            _session.Save(rent1);
            _session.Save(rent2);
            _session.Evict(cust);
            cust = _session.Get<Customer>(cust_Id);
            Assert.AreEqual(rent1, cust.Rentals[0]);
            Assert.AreEqual(rent2, cust.Rentals[1]);
        }

        [Test]
        public void TestReservationsCollectionForMovieMap()
        {
            string movie1_Id = starWars.TitleId;
            Console.Write(movie1_Id);
            Reservation reservation1 = new Reservation()
            {
                Customer = new Customer()
                {
                    Name = nam,
                    StreetAddress = "123 apple st",
                    Phone = "248 909 2112",
                    EmailAddress = "marcoAndCam@yahoo.com",
                    Password = "Woooow121",
                    ZipCode = zeeland
                },
                Movie = starWars,
                ReservationDate = DateTime.Now
            };
            Reservation reservation2 = new Reservation()
            {
                Customer = new Customer()
                {
                    Name = nam,
                    StreetAddress = "123 pear st",
                    Phone = "248 909 2112",
                    EmailAddress = "marcoAndCam@yahoo.com",
                    Password = "Woooow121",
                    ZipCode = zeeland
                },
                Movie = starWars,
                ReservationDate = DateTime.Now
            };

            _session.Save(reservation1);
            _session.Save(reservation2);
            _session.Evict(starWars);
            starWars = _session.Get<Movie>(movie1_Id);
            
            Assert.AreEqual(reservation1, starWars.Reservations[0]);
            Assert.AreEqual(reservation2, starWars.Reservations[1]);
        }

        [Test]
        public void TestPrefersCommTable()
        {
            var customers = new List<Customer>()
            {
                new Customer
                {
                   Name = nam,
                   StreetAddress = "123 pear st",
                   Phone = "248 909 2112",
                   EmailAddress = "marcoAndCam@yahoo.com",
                   Password = "Woooow121",
                   ZipCode = zeeland
                },
                new Customer()
                {
                    Name = nam,
                    StreetAddress = "124 pear st",
                    Phone = "248 909 2112",
                    EmailAddress = "marcoAndCam@yahoo.com",
                    Password = "Woooow121",
                    ZipCode = zeeland
                }
            };

            CommunicationMethod commMethod1 = new CommunicationMethod()
            {
                Name = "Phone",
                Units = CommunicationMethod.TimeUnit.Day,
                Frequency = 5
            };
            CommunicationMethod commMethod2 = new CommunicationMethod()
            {
                Name = "Phone1",
                Units = CommunicationMethod.TimeUnit.Day,
                Frequency = 5
            };

            var commMethods = new List<CommunicationMethod>()
            {
                commMethod1,
                commMethod2
            };

            new PersistenceSpecification<CommunicationMethod>(_session)
                .CheckInverseList(s => s.Customers, customers, (s, v) => v.Allow(s));

            new PersistenceSpecification<Customer>(_session)
                .CheckInverseList(s => s.CommunicationTypes, commMethods, (s, v) => s.Allow(v));
        }

        [Test]
        public void TestRatingToRental()
        {
            Rental rental = new Rental()
            {
                DueDate = DateTime.Now.AddDays(14),
                RentalDate = DateTime.Now,
                ReturnDate = DateTime.Now,
                Customer = new Customer()
                {
                    Name = nam,
                    StreetAddress = "123 apple st",
                    Phone = "248 909 2112",
                    EmailAddress = "marcoAndCam@yahoo.com",
                    Password = "Woooow121",
                    ZipCode = zeeland

                },
                Video = new Video
                {
                    Movie = starWars,
                    NewArrival = false,
                    PurchaseDate = DateTime.Now,
                    Store = new Store
                    {
                        StoreName = "Store 1",
                        StreetAddress = "1260 Winterwood Lane",
                        PhoneNumber = "616-123-4567",
                        ZipCode = zeeland
                    }
                },

            };
            
            Rating rating = new Rating()
            {
                Score = 5,
                Comment = "hello"
            };

            rating.Rental = rental;
            rental.Rating = rating;

            _session.Save(rental);
            _session.Save(rating);

            int rent_Id = rental.Id;
            int rat_Id = rating.Id;

            _session.Evict(rental);
            _session.Evict(rating);

            Rental rentLocal = _session.Get<Rental>(rent_Id);
            Rating ratLocal = _session.Get<Rating>(rat_Id);

            Assert.AreEqual(rating.Id, ratLocal.Id);
            Assert.AreEqual(rental.Id, rentLocal.Id);
        }

        [Test]
        public void TestCustomerToReservation()
        {
            Customer customer = new Customer
            {
                Name = nam,
                StreetAddress = "123 apple st",
                Phone = "248 909 2112",
                EmailAddress = "marcoAndCam@yahoo.com",
                Password = "Woooow121",
                ZipCode = zeeland
            };
            Reservation reservation = new Reservation()
            {
                Movie = starWars,
                ReservationDate = DateTime.Now
            };

            customer.Reservation = reservation;
            reservation.Customer = customer;

            _session.Save(customer);
            _session.Save(reservation);

            int cust_Id = customer.Id;
            int res_Id = reservation.Id;

            _session.Evict(customer);
            _session.Evict(reservation);

            Customer custLocal = _session.Get<Customer>(cust_Id);
            Reservation resLocal = _session.Get<Reservation>(res_Id);

            Assert.AreEqual(customer, custLocal);
            Assert.AreEqual(reservation, resLocal);
        }
    }
}
