﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(31)]
    public class AddMovieIdToReservation : Migration
    {
        public override void Down()
        {
            Delete.Column("Movie_Id")
                 .FromTable("Reservation")
                 .InSchema("videostore");
        }

        public override void Up()
        {
            Alter.Table("Reservation")
                .InSchema("videostore")
                .AddColumn("Movie_Id").AsCustom("char(10)").NotNullable();
        }
    }
}
