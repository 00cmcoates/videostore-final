﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(36)]
    public class AddForeignKeyRentalToCustomer : Migration
    {
        public override void Down()
        {
            Delete.ForeignKey("RentalToCustomer")
                 .OnTable("Rental")
                 .InSchema("videostore");
        }

        public override void Up()
        {
            Create.ForeignKey("RentalToCustomer")
                .FromTable("Rental")
                .InSchema("videostore")
                .ForeignColumn("Customer_Id")
                .ToTable("Customer")
                .InSchema("videostore")
                .PrimaryColumn("Id");
            //                .OnUpdate(System.Data.Rule.Cascade);
        }
    }
}
