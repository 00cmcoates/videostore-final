﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(35)]
    public class AddForeignKeyEmployeeToStore : Migration
    {
        public override void Down()
        {
            Delete.ForeignKey("EmployeeToStore")
                 .OnTable("Employee")
                 .InSchema("videostore");
        }

        public override void Up()
        {
            Create.ForeignKey("EmployeeToStore")
                .FromTable("Employee")
                .InSchema("videostore")
                .ForeignColumn("Store_Id")
                .ToTable("Store")
                .InSchema("videostore")
                .PrimaryColumn("Id")
                .OnUpdate(System.Data.Rule.Cascade);
        }
    }
}
