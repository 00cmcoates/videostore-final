﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(33)]
    public class AddVideoToRental : Migration
    {
        public override void Down()
        {
            Delete.Column("Video")
                          .FromTable("Rental")
                          .InSchema("videostore");
        }

        public override void Up()
        {
            Alter.Table("Rental")
                .InSchema("videostore")
                .AddColumn("Video").AsInt32().NotNullable();
        }
    }
}
