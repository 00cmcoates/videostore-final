﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(25)]
    public class Rating : Migration
    {
        public override void Down()
        {
            Delete.Table("Rating")
                            .InSchema("videostore");
        }

        public override void Up()
        {
            Create.Table("Rating").InSchema("videostore")
                            .WithColumn("Id").AsInt64().Identity().PrimaryKey()
                            .WithColumn("Comment").AsString()
                            .WithColumn("Score").AsInt64();
            //withReference("Rental")
        }
    }
}
