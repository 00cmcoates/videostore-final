﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(4)]
    public class CustomerMigration : Migration
    {
        public override void Down()
        {
            Delete.Table("Customer").InSchema("videostore");
        }

        public override void Up()
        {
            Create.Table("Customer")
                .InSchema("videostore")
                .WithColumn("Id").AsInt32().Identity().PrimaryKey()
                .WithColumn("First").AsString().NotNullable()
                .WithColumn("Middle").AsString().Nullable()
                .WithColumn("Last").AsString().NotNullable()
                .WithColumn("Title").AsString().Nullable()
                .WithColumn("Suffix").AsString().Nullable()
                .WithColumn("StreetAddress").AsString().NotNullable()                
                .WithColumn("Phone").AsString().NotNullable()
                .WithColumn("EmailAddress").AsString().Unique().NotNullable()
                .WithColumn("Password").AsString().NotNullable();
        }
    }
}
