﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(24)]
    public class CommunicationMethodMigration : Migration
    {
        public override void Down()
        {
            Delete.Table("CommunicationMethod")
                .InSchema("videostore");
        }

        public override void Up()
        {
            Create.Table("CommunicationMethod").InSchema("videostore")
                .WithColumn("Name").AsString().PrimaryKey()
                .WithColumn("Frequency").AsInt64()
                .WithColumn("Units").AsString();
            //withList Customers
        }
    }
}
