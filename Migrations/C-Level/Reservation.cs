﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(28)]
    public class Reservation : Migration
    {
        public override void Down()
        {
            Delete.Table("Reservation")
                               .InSchema("videostore");
        }

        public override void Up()
        {
            Create.Table("Reservation").InSchema("videostore")
                .WithColumn("Id").AsInt64().Identity().PrimaryKey()
                .WithColumn("ReservationDate").AsDateTime();

            //with Customer
            //with Movie
        }
    }
}
