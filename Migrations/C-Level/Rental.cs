﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(27)]
    public class Rental : Migration
    {
        public override void Down()
        {
            Delete.Table("Rental")
                              .InSchema("videostore");
        }

        public override void Up()
        {
            Create.Table("Rental").InSchema("videostore")
                .WithColumn("Id").AsInt64().Identity().PrimaryKey()
                .WithColumn("ReturnDate").AsDateTime()
                .WithColumn("RentalDate").AsDateTime()
                .WithColumn("DueDate").AsDateTime();
        }
    }
}
