﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(38)]
    public class CreatePrefersCommTable : Migration
    {
        public override void Down()
        {
            Delete.Table("PrefersComm")
                .InSchema("videostore");
        }

        public override void Up()
        {
            Create.Table("PrefersComm")
                .InSchema("videostore")
                .WithColumn("Id").AsInt32().Identity().PrimaryKey()
                .WithColumn("Customer_Id").AsInt32().NotNullable()
                .WithColumn("CommunicationMethod_Id").AsString().NotNullable();
        }
    }
}
