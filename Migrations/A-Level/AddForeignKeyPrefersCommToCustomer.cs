﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(39)]
    public class AddForeignKeyPrefersCommToCustomer : Migration
    {
        public override void Down()
        {
            Delete.ForeignKey("PrefersCommToCustomer")
                .OnTable("PrefersComm")
                .InSchema("videostore");
        }

        public override void Up()
        {
            Create.ForeignKey("PrefersCommToCustomer")
                .FromTable("PrefersComm")
                .InSchema("videostore")
                .ForeignColumn("Customer_Id")
                .ToTable("Customer")
                .InSchema("videostore")
                .PrimaryColumn("Id")
                .OnDelete(System.Data.Rule.None)
                .OnUpdate(System.Data.Rule.None);
        }
    }
}
