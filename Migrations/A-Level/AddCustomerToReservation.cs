﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(42)]
    public class AddCustomerToReservation : Migration
    {
        public override void Down()
        {
            Delete.Column("Customer_Id")
                  .FromTable("Reservation")
                  .InSchema("videostore");
        }

        public override void Up()
        {
            Alter.Table("Reservation")
                .InSchema("videostore")
                .AddColumn("Customer_Id").AsInt64().NotNullable();
        }
    }
}
