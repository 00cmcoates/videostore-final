﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(21)]
    public class AddForeignKeyPrefersToStore : Migration
    {
        public override void Down()
        {
            Delete.ForeignKey("PrefersToStore")
                .OnTable("Prefers")
                .InSchema("videostore");
        }

        public override void Up()
        {
            Create.ForeignKey("PrefersToStore")
                .FromTable("Prefers")
                .InSchema("videostore")
                .ForeignColumn("Store_Id")
                .ToTable("Store")
                .InSchema("videostore")
                .PrimaryColumn("Id")
                .OnDelete(System.Data.Rule.None)
                .OnUpdate(System.Data.Rule.None);
        }
    }
}
